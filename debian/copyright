Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: collections
Source: https://github.com/juju/collections

Files: *
Copyright: 2013-2015 Canonical Ltd
License: LGPL-3.0-with-exception

Files: debian/*
Copyright:
 Copyright 2018-2021 Shengjing Zhu <i@zhsj.me>
 Copyright 2021-2023 Mathias Gibbens
License: LGPL-3.0-with-exception
Comment: Debian packaging is licensed under the same terms as upstream

License: LGPL-3.0-with-exception
 This software is licensed under the LGPLv3, included below.
 .
 As a special exception to the GNU Lesser General Public License version 3
 ("LGPL3"), the copyright holders of this Library give you permission to
 convey to a third party a Combined Work that links statically or dynamically
 to this Library without providing any Minimal Corresponding Source or
 Minimal Application Code as set out in 4d or providing the installation
 information set out in section 4e, provided that you comply with the other
 provisions of LGPL3 and provided that you meet, for the Application the
 terms and conditions of the license(s) which apply to the Application.
 .
 Except as stated in this special exception, the provisions of LGPL3 will
 continue to comply in full to this Library. If you modify this Library, you
 may apply this exception to your version of this Library, but you are not
 obliged to do so. If you do not wish to do so, delete this exception
 statement from your version. This exception does not (and cannot) modify any
 license terms which apply to the Application, with which you must still
 comply.
 .
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation version 3.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".
